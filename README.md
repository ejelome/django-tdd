django-tdd
==========

Django TDD in 3.0

-------------------------------------------------------------------------------

<!-- markdown-toc start - Don't edit this section. Run M-x markdown-toc-refresh-toc -->
**Table of Contents**

- [django-tdd](#django-tdd)
    - [Prerequisites](#prerequisites)
    - [Setup](#setup)
    - [TDD](#tdd)
        - [Chapter 1](#chapter-1)
        - [Chapter 2](#chapter-2)
    - [Terminologies](#terminologies)
    - [License](#license)

<!-- markdown-toc end -->

-------------------------------------------------------------------------------

Prerequisites
-------------

- [Pipenv](https://github.com/pypa/pipenv)

-------------------------------------------------------------------------------

Setup
-----

1. Create and move to project directory:

   ``` shell
   $ \
   mkdir -p ~/Projects/djangotdd
   cd ~/Projects/djangotdd
   ```

2. Create a virtual environment:

   ``` shell
   $ pipenv --python 3.7
   ```

3. Activate virtual environment:

   ``` shell
   $ pipenv shell
   ```

-------------------------------------------------------------------------------

TDD
---

### Chapter 1 ###

1. Create a test file:

   ``` shell
   $ echo "
   from selenium import webdriver

   browser = webdriver.Chrome()
   browser.get('http://localhost:8000')

   assert 'Django: the Web framework for perfectionists with deadlines.' in browser.title

   browser.close()
   " > functional_tests.py
   ```

2. Execute the test file:

   ``` shell
   $ python functional_tests.py
      Traceback (most recent call last):
     File "functional_tests.py", line 2, in <module>
       from selenium import webdriver
   ModuleNotFoundError: No module named 'selenium'
   ```

3. Install `selenium`:

   ``` shell
   $ pipenv install --dev selenium
   ```

   **Notes:**

   - The `--dev` installs the package as a **development** package
   - Development packages are only used in a local environment

4. Re-execute the test file:

   ``` shell
   $ python functional_tests.py
   ```

   Close the browser, it should prompt the following:

   ``` shell
   Traceback (most recent call last):
     File "functional_tests.py", line 7, in <module>
       assert 'Django: the Web framework for perfectionists with deadlines.' in browser.title
   AssertionError
   ```

5. Create a Django project:

   ``` shell
   $ django-admin startproject djangotdd
      The program 'django-admin' is currently not installed. You can install it by typing:
   sudo apt install python-django-common
   ```

   **Note:** This is one of those rare cases where it's not correct to follow the provided help as the solution.

6. Install Django:

   ``` shell
   $ pipenv install django
   ```

7. Retry creating a Django project:

   ``` shell
   $ django-admin startproject djangotdd
   ```

8. Re-execute the test file:

   ``` shell
   $ python functional_tests.py
   ```

   Close the browser, it should still prompt the following:

   ``` shell
   Traceback (most recent call last):
     File "functional_tests.py", line 7, in <module>
       assert 'Django: the Web framework for perfectionists with deadlines.' in browser.title
   AssertionError
    ```

    **Hint:** The URL we're contacting is a unreachable, hence, no response which breaks the test.

9. Move to and run Django's web server:

   ``` shell
   $ \
   cd djangotdd
   ./manage.py runserver
   ```

10. Open a new terminal and re-execute the test file:

    1. Activate virtual environment:

       ``` shell
       $ pipenv shell
       ```

    2. Re-execute the test file:

       ``` shell
       $ python functional_tests.py
       ```

    The browser should close automatically and prompt no errors.

### Chapter 2 ###

**Functional Test with comments**

``` python
from selenium import webdriver

# John wants to check the app he saw online, so he opens the browser:
browser = webdriver.Chrome()

# He entered the URL "localhost:8000" then pressed enter:
browser.get('http://localhost:8000')

# He then saw the long title 'Django: the Web framework for perfectionists with deadlines.':
assert 'Django: the Web framework for perfectionists with deadlines.' in browser.title

# He was fascinated on what he saw, so he closed his browser:
browser.close()
```

**Functional Test with `unittest` **

``` python
import unittest

from selenium import webdriver


class NewVisitorTest(unittest.TestCase):
    def setUp(self):
        # John wants to check the app he saw online, so he opens the browser:
        self.browser = webdriver.Chrome()

    def tearDown(self):
        # He was fascinated on what he saw, so he closed the browser:
        self.browser.close()

    def test_new_visitor_visits_url(self):
        # He entered the URL 'localhost:8000' then pressed enter:
        self.browser.get('http://localhost:8000')

        # He then saw the long title:
        # 'Django: the Web framework for perfectionists with deadlines.':
        self.assertIn(
            'Django: the Web framework for perfectionists with deadlines.',
            self.browser.title
        )

        # TODO:
        self.fail('WIP')


if __name__ == '__main__':
    unittest.main()
```

**Notes:**

- In `unittest`, tests are grouped in classes &mdash; inheriting from `TestCase`
- By convention, test classes are suffixed with `Test` and methods prefixed with `test_`
- `setUp()` is used to prepare test fixtures; it is run _before_ running each tests
- `tearDown()` is used to perform cleanup; it is run _after_ running each tests
- `assertIn` is one of the [`assert methods`](https://docs.python.org/3/library/unittest.html#assert-methods)
- Remember to use `self` when using assert methods

-------------------------------------------------------------------------------

Terminologies
-------------

- [Functional Test](https://en.wikipedia.org/wiki/Functional_testing) is [Acceptance Test](https://en.wikipedia.org/wiki/Acceptance_testing) is End-to-End Test (or E2E)

  - This tests the whole application according to the specifications and user expectations
  - A related test is called [Black-box testing](https://en.wikipedia.org/wiki/Black-box_testing) to test the application without knowing any of its internal workings
  - It should have a human-readable story that we can follow (e.g. User Stories)

- [User Story](https://en.wikipedia.org/wiki/User_story)

  - A step-by-step scenario of a how a user will use the system (or a feature)
  - It is intended for end user's point of view (e.g. customers or developers themselves)

- [MVP](https://en.wikipedia.org/wiki/Minimum_viable_product) (Minimum Viable Product)

  - The simplest form of the program that can be built that is already useful to customers
  - Just enough features to satisfy early customers to get feedback for further development

-------------------------------------------------------------------------------

License
-------

`django-tdd` is licensed under [MIT](./LICENSE)
